/**
 * File: main.go
 * Written by: Stephen M. Reaves
 * Created on: Fri, 18 Mar 2022
 */

package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"text/template"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/reavessm/goblog/pkg/config"
	"gopkg.in/yaml.v2"
)

type model struct {
	cfg     *config.Config
	srv     *http.ServeMux
	htmlMap map[string][]byte
}

func main() {
	if err := run(); err != nil {
		log.Println("Error! ", err)
		os.Exit(1)
	}
}

func getBlogFiles(dir string) ([]config.Blog, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	var fileNames []config.Blog
	for _, fileInfo := range files {
		name := fileInfo.Name()
		splits := strings.Split(name, ".")
		if len(splits) < 2 {
			return nil, errors.New("BAD")
		}

		file, err := os.Open(config.BlogDir + "/" + name)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("Cannot open file: %v", err))
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)

		count := 0
		inComment := 0
		s := ""
		for scanner.Scan() {
			text := scanner.Text()

			if strings.Contains(text, "<!--") {
				inComment++
			}
			if inComment > 0 {
				if strings.Contains(text, config.DescriptionKeyword) {
					s += strings.TrimPrefix(text, config.DescriptionKeyword) + "\n"
					count++
				}
			}
			if strings.Contains(text, "-->") {
				inComment--
			}

		}

		prefix := splits[0]
		suffix := splits[1]

		splitss := strings.Split(prefix, "_")
		if len(splitss) < 2 {
			return nil, errors.New("BAD")
		}
		date := splitss[0]
		blogName := splitss[1]
		if suffix == "md" {
			fileNames = append(fileNames, config.Blog{
				Name:        blogName,
				Date:        date,
				Description: s,
			})
		}
	}
	return fileNames, nil
}

func (m *model) readYaml(path string) error {
	yfile, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	cfg := &config.Config{}

	err = yaml.Unmarshal(yfile, cfg)
	if err != nil {
		return err
	}

	b, err := getBlogFiles(config.BlogDir)
	if err != nil {
		return err
	}

	cfg.Blogs = b
	m.cfg = cfg

	return nil
}

func parseHTML(fileName string) ([]byte, error) {
	contents, readErr := ioutil.ReadFile(fileName)
	if readErr != nil {
		return []byte(""), readErr
	}
	opts := html.RendererOptions{
		Flags: html.CompletePage | html.CommonFlags | html.HrefTargetBlank,
		CSS:   config.CSSFile,
		Icon:  config.IconFile,
	}
	return markdown.ToHTML(contents, nil, html.NewRenderer(opts)), nil
}

func (m *model) handleBlogs() error {
	for k, v := range m.htmlMap {
		m.blogHandleFunc(k, v)
	}
	return nil
}

func (m *model) blogHandleFunc(name string, data []byte) {
	m.srv.HandleFunc(name, func(rw http.ResponseWriter, r *http.Request) {
		rw.Write(data)
	})
}

func (m *model) parseBlogs() error {
	basedir := config.BlogDir
	for _, v := range m.cfg.Blogs {
		blogName := fmt.Sprintf("/%s_%s", v.Date, v.Name)

		html, err := parseHTML(basedir + blogName + ".md")
		if err != nil {
			return err
		}
		m.htmlMap[blogName] = html
	}

	return nil
}

func run() error {
	m := &model{htmlMap: make(map[string][]byte)}

	// Parse before handle for perf reasons
	log.Println("Parsing TOC")
	if err := m.readYaml(config.ConfigFile); err != nil {
		return err
	}
	tmpl := template.Must(template.ParseFiles(config.TemplateFile))

	// Parse before handle for perf reasons
	log.Println("Parsing blogs")
	if err := m.parseBlogs(); err != nil {
		return err
	}

	log.Println("Starting web server")
	m.srv = http.NewServeMux()

	m.srv.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		tmpl.Execute(rw, m.cfg)
	})

	m.srv.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./pkg/template/static/"))))

	log.Println("Generating blog endpoints")
	if err := m.handleBlogs(); err != nil {
		return err
	}

	log.Println("Listening on port ", config.PortNumber)
	return http.ListenAndServe(fmt.Sprintf(":%s", config.PortNumber), m.srv)
}
