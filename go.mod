module github.com/reavessm/goblog

go 1.16

replace github.com/reavessm/goblog => /home/sreaves/Src/GoBlog

require (
	github.com/gomarkdown/markdown v0.0.0-20220310201231-552c6011c0b8
	gopkg.in/yaml.v2 v2.4.0
)
