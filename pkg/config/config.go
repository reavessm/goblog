package config

const (
	PortNumber         string = "8080"
	BlogDir            string = "blogs"
	CSSFile            string = "/static/stylesheet.css"
	IconFile           string = "/static/favicon.ico"
	ConfigFile         string = "pkg/config/config.yml"
	TemplateFile       string = "pkg/template/toc.tmpl"
	DescriptionKeyword string = "Description: "
)

type Blog struct {
	Name        string `yaml:"name"`
	Date        string `yaml:"date"`
	Description string `yaml:"description"`
}

type Config struct {
	Title       string `yaml:"title"`
	Description string `yaml:"description"`
	Blogs       []Blog `yaml:"blogs"`
}
