<!--
File: 20220330_goblog.md
Written by: Stephen M. Reaves
Created on: Wed, 30 Mar 2022
Description: Markdown + Go => blog
-->

# GoBlog

aka I need better names

## What is it?

[goblog](https://gitlab.com/reavessm/goblog) is tool I wrote to generate this blog platform.

## Why?

cuz

## How

I'm glad you asked.  Lets take a quick look at the tree:

```
.
├── blogs
│   ├── 20220321_OSB.md
│   ├── 20220322_fedoraRouterIdeas.md
│   └── 20220322_OBI.md
├── cmd
│   └── goblog
│       └── main.go
├── Containerfile
├── go.mod
├── go.sum
├── Makefile
└── pkg
    ├── config
    │   ├── config.go
    │   └── config.yml
    └── template
        ├── static
        │   └── stylesheet.css
        └── toc.tmpl

7 directories, 12 files
```

Seems like a standard golang project structure.  We have our main in `cmd`, some config stuffs in `pkg/config` and some templates in `pkg/template`.  Cool.

Basically, you just type `make blog` and you pulled into a text editor.  There you can create the markdown file as you normally would.  When you're done, you can hit build to creates the binary, clean to removes the binary, serve to build and run the binary, or new/blog to create a new blog.  When running the binary, goblog will automagically convert all the markdown files into html, then create a TOC file using templates.  It's quite similar to [portfoligo](https://github.com/reavessm/portfoligo).

## Is that all the Secret Sauce?

NO!  Who runs code by itself anymore?  I have a containerfile in the repo that can package everything up into a container, super simple.  Then I create an application in Openshift to pull from the repo on push events, build the container locally, push to an imagestream, upgrade a deployment to the latest imagestream, thus updating my blog for the public, not so simple.

## So what's the workflow?

When I have an idea, I type `make new`, then I write my blog.  Then when I'm done, I simply git add/commit/push, and Openshift takes care of the rest.  At some point, my blog site will be updated with my new content.

[toc](/)
