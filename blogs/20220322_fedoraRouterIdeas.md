<!--
File: 20220322_fedoraRouterIdeas.md
Written by: Stephen M. Reaves
Created on: Tue, 22 Mar 2022
Description: Using Fedora as a router for my home network
-->

This is an idea that I've had kicking around in my head for a while now.  I don't have all the details worked out, but I'm going to do a brain dump here and see what comes out

# Current Infra

Right now, I am running a USG Pro 4, as well as various other unifi switches, APs, etc.  The USG is aging a bit and is closed software.  There's a very limited set of things you can do with their routers.  This can be seen as a good thing, but it doesn't alllow you to tinker very much.  I'm also running things like [PiHole](https://pi-hole.net/) on my VM cluster (oVirt/OpenShift), but my VM hosts need PiHole running in order to do certain things.  I am also running the unifi controller as a VM on the same cluster and I'm having the same issues.  This is ignoring the fact that oVirt is going away.  I **NEED** to rebuild my network infrastructure.

# Potential Plan

What if I ditch my USG and run a plain jane Fedora server as a router?  [It's been done before](https://fedoramagazine.org/use-fedora-server-create-router-gateway/), but most guides are for older versions of Fedora.  That *shouldn't* be an issue, but I can't know that until I try it out.

## Why Fedora?

Mostly because I'm familiar with Fedora (I use it for my day job as a Software Engineer at Red Hat), but Fedora does have quite a few niceties over other linux distros

### SELinux

I know, I know, [SELinux is a *pain*](https://www.reddit.com/r/linuxadmin/comments/alh22l/i_get_that_selinux_is_a_pain_to_deal_with_but_we/) to say the least, but this is for a reason.  Learning to master SELinux will be a necessity because of the security it provides.  There have been numerous CVEs that were stopped by SELinux.  If this box is going to be my first line of defense, it needs to be as secure as possible, full stop.

### Podman

I mentioned before that I was running PiHole as a VM, but PiHole could just as easily run as container.  Running PiHole and other network infrastructure components as containers on my router just makes sense.  Having things like [HomeAssistant](https://www.home-assistant.io/) and even potentially the unifi controller (if its still needed for the switches, etc.) separate from other infrastructure will allow me a greater level of flexibility in my home network and home lab.  Podman can even generate systemd unit files and have the containers auto start on boot

### Cockpit

I love [cockpit](https://cockpit-project.org/).  Its a beautiful, functional UI that will fit in with existing Fedora/RH programs.  Cockpit has different tabs to manage network, storage, containers, and other parts of the system.

### VMs

If, for some reason, there is something that cannot be run as container, I can still use the same [QEMU/KVM](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/) virtualization technology that I'm used to.  There is even a place to manage them in Cockpit.

### Recent Updates

Fedora seems to strike a good balance between fast updates and stability.  I could go with something like Arch to get more up to date packages at the risk of breaking things, or I could go with Debian for ultimately stability at the cost of older packages, but I think Fedora does a good job of being in the middle.

### BTRFS

Fedora now comes standard with [BTRFS](https://fedoraproject.org/wiki/Btrfs) as its default file system.  This can provide several advance features like compression and snapshots which should make this router fast and incredibly stable.

## How to Migrate

As the internet is quite vital to my home network, it will be quite difficult for me to just replace my router on a whim.  I also want to make sure everything is working BEFORE I make the switch (well, I'm making a router, bad choice of words).

[toc](/)
