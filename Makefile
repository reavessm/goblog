help:
	@echo "usage: make <build | clean | serve | new>"
	@echo
	@echo "       build: Creates static binary"
	@echo "       clean: Removes static binary"
	@echo "       serve: Builds and runs binary"
	@echo "       new:   Opens new markdown file to create new blog"
	@echo "       blog:  Alias for new"

.PHONY: help

serve:
	go run cmd/goblog/main.go

.PHONY: serve

build:
	go build -a -ldflags '-extldflags "-static"' -o goblog cmd/goblog/main.go

.PHONY: build

clean:
	rm -rvf ./goblog

.PHONY: clean

new:
	$(eval NAME=$(shell read -p "Please enter blog title (no spaces) " n; echo $$n))
	$(eval DATE=$(shell date +%Y%m%d))
	$(eval FILE=blogs/$(DATE)_$(NAME).md)
	@vim $(FILE)
	@echo -e "\n[toc](/)" >> $(FILE)

.PHONY: new

blog: new

.PHONY: blog
